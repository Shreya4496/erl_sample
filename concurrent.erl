-module(helloworld). 
-export([loop/0,start/0]). 

loop() ->
   receive 
      {multiply, X, Y} -> 
         io:fwrite("Multiplication is ~p~n" ,[X*Y]), 
         loop(); 
      {add,X,Y} ->
         io:fwrite("Addition is ~p~n" , [X+Y]), 
         loop(); 
      {subtract,X,Y} ->
      io:fwrite("Subtraction is ~p~n" , [X-Y]), 
      loop(); 
      {division,X,Y} ->
      io:fwrite("Division is ~p~n" , [X/Y]), 
      loop(); 
   Other ->
      io:fwrite("Unknown"), 
      loop() 
   end. 

start() ->
   Pid = spawn(fun() -> loop() end), 
   Pid ! {division, 6, 10},
   Pid1 = spawn(fun() -> loop() end), 
   Pid1 ! {multiply, 6, 10},
   Pid2 = spawn(fun() -> loop() end), 
   Pid2 ! {add, 6, 10},
   Pid3 = spawn(fun() -> loop() end), 
   Pid3 ! {subtract, 6, 10}.